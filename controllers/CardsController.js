'use strict';
const mongoose = require('mongoose');
const Rows = mongoose.model('Row');

class Cards {
  get() {
    return Rows.find({});
  }
  add() {
    return new Rows({
      cards: [{}]
    });
  }
  delete(req) {
    return Rows.findOneAndRemove({
      _id: req.params.id
    });
  }

  getCards(req) {
    return Rows.findById(req.params.id, 'cards');
  }
  addCard(req) {
    return Rows.findOneAndUpdate({
      _id: req.params.id
    }, {
      $push: {
        cards: {}
      }
    }, {
      new: true
    });
  }
  deleteCard(req) {
    return Rows.findOneAndUpdate({
      _id: req.params.id
    }, {
      $pull: {
        cards: { _id: req.params.card_id }
      }
    }, {
      new: true
    });
  }
  saveCard(req) {
    return Rows.findOneAndUpdate({
      _id: req.params.id,
      'cards._id': req.params.card_id
    }, {
      $set: {
        'cards.$.text': req.params.text,
        'cards.$.visible': true,
      }
    }, {
      new: true
    });
  }
}

module.exports = new Cards();
