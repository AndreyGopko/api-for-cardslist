const express = require('express');
const router = module.exports = express.Router();
const Cards = require('../controllers/CardsController.js');

router.get('/', (req, res, next) => {
  Cards.get().exec((err, mess) => (err ? next(err) : res.json(mess)));
});
router.post('/', (req, res, next) => {
  Cards.add().save((err, mess) => (err ? next(err) : res.send(mess)));
});
router.delete('/:id', (req, res, next) => {
  Cards.delete(req).exec((err, mess) => (err ? next(err) : res.send(mess)));
});
router.get('/:id/cards', (req, res, next) => {
  Cards.getCards(req).exec((err, mess) => (err ? next(err) : res.send(mess.cards)));
});
router.post('/:id/cards', (req, res, next) => {
  Cards.addCard(req).exec((err, mess) => (err ? next(err) : res.send(mess.cards)));
});
router.delete('/:id/cards/:card_id', (req, res, next) => {
  Cards.deleteCard(req).exec((err, mess) => (err ? next(err) : res.send(mess.cards)));
});
router.put('/:id/cards/:card_id/:text', (req, res, next) => {
  Cards.saveCard(req).exec((err, mess) => (err ? next(err) : res.send(mess)));
});
