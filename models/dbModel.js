const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const cardsSchema = new Schema({
  createDate: {
    type: Date,
    default: Date.now
  },
  text: String,
  visible: { type: Boolean, default: false }
});
const rowSchema = new Schema({
  cards: [cardsSchema],

});
mongoose.model('Row', rowSchema);
