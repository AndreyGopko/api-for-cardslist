const mongoose = require('mongoose');
const glob = require('glob');
const config = require('../config');

const url = config.mongoose.uri;
mongoose.connect(url);
glob.sync('../models/**.js', {
  cwd: __dirname
}).forEach(require);

require('../models/dbModel');
